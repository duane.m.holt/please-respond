/**
 * A Transform stream that summarizes a collection of Meetup RSVPs.
 * @typedef { Object } RsvpSummaryStream
 * @property { number } total - the total number of RSVPs consumed by the stream
 * @property { Map } countryCounts - a map country counts tallying the number of
 *                   unique countries found in the RSVP groups
 * @property { number } future_date - the date of the RSVP event furthest into
 *                      the future in milliseconds since epoch
 * @property { string } future_url - the url of the RSVP event furthest into the
 *                      future
 */
const { Transform } = require('stream');
const util = require('util');
const { format } = require('date-fns');
const { CountrySummary, reverseCount } = require('./CountrySummary.js');

/**
 * Constructs a new RsvpSummaryStream.
 */
function RsvpSummaryStream() {
  Transform.call(this, { writableObjectMode: true });
  
  this.total = 0;
  this.countryCounts = new Map();
  this.future_date = 0;
  this.future_url = '';
}
util.inherits(RsvpSummaryStream, Transform);

/**
 * Updates this summary with the rsvp object. This method accumulates summary
 * information about RSVPs in the stream. Data is not emitted until this stream
 * ends.
 * 
 * @param { Object } rsvp - the rsvp json object to process
 * @param { string } isEncoded - ignored
 * @param { function } getNextRsvp - callback function to run after the rsvp has
 *                     been processed
 */
RsvpSummaryStream.prototype._transform = function(rsvp, isEncoded, getNextRsvp) {
  this.summarizeEvent(rsvp.event);
  this.summarizeGroup(rsvp.group);
  // Do not push after each transform.  Only push the final summary.
  getNextRsvp();
};

/**
 * Flush the summary data from this stream.
 * @param { function } done - the callback function to execute when this stream's
 *                     summary data has been flushed
 */
RsvpSummaryStream.prototype._flush = function(done) {
  const summary = this.toString();
  this.push(`${summary}\n`);
  this.push(null);
  done();
};

/**
 * Summarizes event data from an RSVP.
 * @param { Object } event - an RSVP event
 */
RsvpSummaryStream.prototype.summarizeEvent = function(event) {
  this.total++;
  if (this.future_date < event.time) {
    this.future_date = event.time;
    this.future_url = event.event_url;
  }
};

/**
 * Summarizes group data from an RSVP.
 * @param { Object } group - an RSVP group
 */
RsvpSummaryStream.prototype.summarizeGroup = function(group) {
  // Update the number of times a country appeared in the stream
  let count = this.countryCounts.get(group.group_country);
  // Increment the count if it already found, otherwise initialize the count
  count = count ? count + 1 : 1;
  this.countryCounts.set(group.group_country, count);    
};

/**
 * Returns a string representing the RSVP summary of this stream.
 */
RsvpSummaryStream.prototype.toString = function() {
  const dateStr = format(this.future_date, 'YYYY-MM-DD HH:mm');
  const countryStr = this.formatCountries();

  return `${this.total},${dateStr},${this.future_url},${countryStr}`;
};

/**
 * Returns a string representing the top three countries processed by this stream.
 */
RsvpSummaryStream.prototype.formatCountries = function() {
  const countrySummaries = Array.from(this.countryCounts.entries())
                                .map((entry) => new CountrySummary(entry[0], entry[1]));

  const top3 = countrySummaries.sort(reverseCount)
                               .filter(limit(3));

  return top3.map(summary => summary.toString()).join(',');
};

/**
 * Returns a filter function that limits the number of returned elements
 * to the max number specified.
 *
 * @param { number } max - the maximum number of elements to return
 * @return { function } the filter function
 *
 * @example
 *
 *   const firstThree = ary.filter(limit(3));
 */
function limit(max) {
  return (_elem, index) => index < max;
}

module.exports = {
  RsvpSummaryStream,
  limit
};

