/**
 * A summary of country information from a Meetup group.
 * @typedef { Object } CountrySummary
 * @property { string } code - the 2-character country code
 * @property { number } count - the country's count
 */

/**
 * Constructs a new CountrySummary instance.
 *
 * @param { string } code - the 2-character country code
 * @param { number } count - the country's count
 */
function CountrySummary(code, count) {
  this.code = code;
  this.count = count;
}

/**
 * Returns a string representing this CountrySummary
 */
CountrySummary.prototype.toString = function() {
  return `${this.code},${this.count}`;
};

/**
 * Function for sorting an array of CountrySummary objects in reverse
 * order by count.
 *
 * @param { CountrySummary } a - the first element to compare
 * @param { CountrySummary } b - the second elementn to compare
 * @return { boolean } -1, 0, or 1 if the b.count is less than, equal to, or
 *                     greater than a.count
 *
 * @example
 *
 *   const sorted = countrySummaryArray.sort(reverseCount);
 */
function reverseCount(a, b) {
  return (b.count < a.count)
      ? -1
      : ((b.count > a.count) ? 1 : 0)
}

module.exports = {
  CountrySummary,
  reverseCount
};

