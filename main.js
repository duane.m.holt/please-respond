const request = require('request');
const createScraper = require('json-scrape');
const { format } = require('date-fns');
const { RsvpSummaryStream } = require('./lib/RsvpSummaryStream.js');

// Read polling duration in seconds from the command line.
// Default to 60 seconds if duration is not specified.
let durationInSeconds = process.argv[2] ? process.argv[2] : 60;

const req = request.get('http://stream.meetup.com/2/rsvps'); 
const scraper = createScraper();
const summary = new RsvpSummaryStream();

req.pipe(scraper)         // parse the response body as json
   .pipe(summary)         // summarize rsvps
   .pipe(process.stdout); // display the summary to standard out

setTimeout(() => {
  // Stop polling data
  req.abort();

  // Flush summary data
  summary.end();
}, durationInSeconds * 1000);

