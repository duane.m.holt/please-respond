const { RsvpSummaryStream, limit } = require('../lib/RsvpSummaryStream.js');
const { format } = require('date-fns');

describe('RsvpSummaryStream', function() {
  let stream;
  let observer;
  let event;
  let group;
  let rsvp;
  let callback;

  beforeEach(function() {
    stream = new RsvpSummaryStream();
    observer = { callback: function() {} };

    event = { time: 10000, event_url: 'eventUrl' };
    group = { group_country: 'us' };
    rsvp = { event: event, group: group };

    callback = jasmine.createSpy('callback');
  });

  describe('constructor', function() {
    it('should create a new instance', function() {
      expect(stream).toBeDefined();
    });

    it('should initialize summary properties', function() {
      expect(stream.total).toEqual(0);

      expect(stream.future_date).toEqual(0);
      expect(stream.future_url).toEqual('');

      expect(stream.countryCounts).toBeDefined();
      expect(stream.countryCounts.size).toEqual(0);
    });
  });

  describe('_transform', function() {
    beforeEach(function() {
      spyOn(stream, 'push');
      spyOn(stream, 'summarizeEvent');
      spyOn(stream, 'summarizeGroup');

      stream._transform(rsvp, '', callback);
    });

    it('should not push data', function() {
      expect(stream.push).not.toHaveBeenCalled();
    });

    it('should call summarizeEvent with rsvp.event', function() {
      expect(stream.summarizeEvent).toHaveBeenCalledWith(event);
    });

    it('should call summarizeGroup with rsvp.event', function() {
      expect(stream.summarizeGroup).toHaveBeenCalledWith(group);
    });

    it('should execute the callback function', function() {
      expect(callback).toHaveBeenCalled();
    });
  });

  describe('_flush', function() {
    beforeEach(function() {
      spyOn(stream, 'push');
      spyOn(stream, 'toString').and.returnValue('summary');

      stream._flush(callback);
    });

    it('should push the summary', function() {
      expect(stream.push).toHaveBeenCalledWith('summary\n');
    });

    it('should end the stream', function() {
      expect(stream.push).toHaveBeenCalledWith(null);
    });

    it('should execute the callback function', function() {
      expect(callback).toHaveBeenCalled();
    });
  });

  describe('summarizeEvent', function() {
    beforeEach(function() {
      stream.summarizeEvent(event);
    });
    
    it('should update the total number of RSVPs processed', function() {
      expect(stream.total).toEqual(1);
    });

    it('should update the future date and url when the event is further in the future', function() {
      expect(stream.future_date).toEqual(10000);
      expect(stream.future_url).toEqual('eventUrl');
    });

    it('should not update the future date and url when the event is earlier than the latest event', function() {
      stream.summarizeEvent({ time: 5000, event_url: 'earlier than current' });

      expect(stream.future_date).toEqual(10000);
      expect(stream.future_url).toEqual('eventUrl');
    });
  });

  describe('summarizeGroup', function() {
    beforeEach(function() {
      stream.summarizeGroup(group);
    });

    it('should initialize the country count when it is first detected', function() {
      expect(stream.countryCounts.get('us')).toEqual(1);
    });

    it('should increment an existing country count', function() {
      stream.summarizeGroup(group);
      expect(stream.countryCounts.get('us')).toEqual(2);
    });
  });

  describe('toString', function() {
    it('should return a formatted summary string', function() {
      const date = new Date(2019, 4, 1, 8, 30);
      const expectedDateStr = format(date, 'YYYY-MM-DD HH:mm');
      const expected = `15,${expectedDateStr},future_url,five,5,four,4,three,3`;

      stream.total = 15;
      stream.future_date = date.getTime();
      stream.future_url = 'future_url';
      stream.countryCounts.set('one', 1);
      stream.countryCounts.set('two', 2);
      stream.countryCounts.set('three', 3);
      stream.countryCounts.set('four', 4);
      stream.countryCounts.set('five', 5);

      expect(stream.toString()).toEqual(expected);
    });
  });
});

