const { CountrySummary, reverseCount } = require('../lib/CountrySummary.js');

describe('CountrySummary', function() {
  let countrySummary;

  beforeEach(function() {
    countrySummary = new CountrySummary('us', 5);
  });

  describe('constructor', function() {
    it('should construct a new instance', function() {
      expect(countrySummary).toBeDefined();
    });

    it('should accept countryCode and count as parameters', function() {
      expect(countrySummary.code).toEqual('us');
      expect(countrySummary.count).toEqual(5);
    });
  });

  describe('toString', function() {
    it('should return a comma delimited string of country code and count', function() {
      expect(countrySummary.toString()).toEqual('us,5');
    });
  });

  describe('reverseCount', function() {
    let other;

    beforeEach(function() {
      other = new CountrySummary('ca', 1);
    });

    it('should return -1 when a.count > b.count', function() {
      expect(reverseCount(countrySummary, other)).toEqual(-1);
    });

    it('should return 1 when a.count < b.count', function() {
      expect(reverseCount(other, countrySummary)).toEqual(1);
    });

    it('should return 0 when a.count = b.count', function() {
      other.count = countrySummary.count;
      expect(reverseCount(countrySummary, other)).toEqual(0);
    });
  });
});

